package hr.fer.nos.lab2.jfx.component.choice;

import hr.fer.nos.lab2.supported.AsymmetricAlgorithmChoice;
import javafx.beans.NamedArg;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;

public class AsymmetricAlgorithmInput extends TitledPane {

    private static final String TEXT = "Kriptografski algoritam";
    private static final String LABEL_TEXT = "Odaberi:";

    private ChoiceBox<AsymmetricAlgorithmChoice> choiceBox = new ChoiceBox<>();

    public AsymmetricAlgorithmInput(@NamedArg(value = "text", defaultValue = TEXT) String text,
            @NamedArg(value = "labelText", defaultValue = LABEL_TEXT) String labelText) {
        choiceBox.getItems().setAll(AsymmetricAlgorithmChoice.values());
        choiceBox.setValue(AsymmetricAlgorithmChoice.RSA_1024);

        HBox hBox = new HBox(10.0, new Label(labelText), choiceBox);
        hBox.setAlignment(Pos.CENTER_LEFT);

        setText(text);
        setContent(hBox);
    }

    public AsymmetricAlgorithmChoice getChosenValue() {
        return choiceBox.getValue();
    }
}
