package hr.fer.nos.lab2.jfx.component.file;

import java.nio.file.Paths;

public class EnvelopeSignatureInput extends FileInput {

    public EnvelopeSignatureInput() {
        super("Potpis omotnice", Paths.get("./signed_envelope_signature.txt"));
    }
}
