package hr.fer.nos.lab2.jfx.util;

import javafx.stage.FileChooser;

import java.io.File;
import java.nio.file.Path;

public final class FileChooserUtil {

    private FileChooserUtil() {
    }

    public static Path open(String name) {
        return openSave(name, false);
    }

    public static Path save(String name) {
        return openSave(name, true);
    }

    public static Path[] saveMultiple(String... names) {
        Path[] paths = new Path[names.length];

        for (int i = 0; i < names.length; i++) {
            Path path = save(names[i]);
            if (path == null) return null;

            paths[i] = path;
        }

        return paths;
    }

    private static Path openSave(String name, boolean save) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialFileName(name);
        File file = openSaveFile(fileChooser, save);
        return file == null ? null : file.toPath();
    }

    private static File openSaveFile(FileChooser fileChooser, boolean save) {
        return save ? fileChooser.showSaveDialog(null) : fileChooser.showOpenDialog(null);
    }
}
