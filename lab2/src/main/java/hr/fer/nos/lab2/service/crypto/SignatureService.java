package hr.fer.nos.lab2.service.crypto;

import hr.fer.nos.lab2.util.Logger;

import javax.crypto.Cipher;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;

import static hr.fer.nos.lab2.supported.AsymmetricAlgorithm.RSA;
import static org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME;

/**
 * Singleton service used for digital signature generation/verification.
 */
public class SignatureService {

    private static final String PADDING = "PKCS1Padding";

    private static SignatureService instance = new SignatureService();

    public static SignatureService getInstance() {
        return instance;
    }

    private SignatureService() {
    }

    /**
     * Calculates the hash of the given message using the specified hashing
     * algorithm and encrypts it using the specified private key.
     *
     * @param message       The given message.
     * @param hashAlgorithm The specified hashing algorithm.
     * @param privateKey    The specified private key.
     * @return The message signature.
     */
    public byte[] generateRSASignature(byte[] message, String hashAlgorithm,
            PrivateKey privateKey) {
        Logger.getInstance().log("Generating signature...");

        try {
            Logger.getInstance().logStep("Generating hash...");
            byte[] hash = MessageDigest.getInstance(hashAlgorithm).digest(message);

            Logger.getInstance().logStep("Encrypting and padding...");
            Cipher c = Cipher.getInstance(RSA.label + "/None/" + PADDING, PROVIDER_NAME);
            c.init(Cipher.ENCRYPT_MODE, privateKey);
            return c.doFinal(hash);
        } catch (GeneralSecurityException e) {
            throw new CryptoServiceException(e.getMessage(), e);
        }
    }

    /**
     * Decrypts the given signature using the specified public key and compares
     * its hash to the hash of the given message.
     *
     * @param signature     The given signature.
     * @param hashAlgorithm The specified hashing algorithm.
     * @param message       The given message.
     * @param publicKey     The specified public key.
     * @return {@code true} if the signature is valid for the given message.
     */
    public boolean verifyRSASignature(byte[] signature, String hashAlgorithm,
            byte[] message, PublicKey publicKey) {
        Logger.getInstance().log("Verifying signature...");

        try {
            Cipher c = Cipher.getInstance(RSA.label + "/None/" + PADDING, PROVIDER_NAME);
            c.init(Cipher.DECRYPT_MODE, publicKey);

            Logger.getInstance().logStep("Decrypting signature...");
            byte[] actual;
            try {
                actual = c.doFinal(signature);
            } catch (Exception e) {
                Logger.getInstance().logStep("Error occurred: " + e.getMessage());
                return false;
            }

            Logger.getInstance().logStep("Generating hash...");
            byte[] expected = MessageDigest.getInstance(hashAlgorithm).digest(message);

            Logger.getInstance().logStep("Comparing hashes...");
            return Arrays.equals(expected, actual);
        } catch (GeneralSecurityException e) {
            throw new CryptoServiceException(e.getMessage(), e);
        }
    }
}
