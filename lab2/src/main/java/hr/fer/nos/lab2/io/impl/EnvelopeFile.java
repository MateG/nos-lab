package hr.fer.nos.lab2.io.impl;

import hr.fer.nos.lab2.io.CryptoFile;
import hr.fer.nos.lab2.io.CryptoFileDescription;
import hr.fer.nos.lab2.io.CryptoFileField;
import hr.fer.nos.lab2.io.CryptoFileProperties;
import hr.fer.nos.lab2.supported.AsymmetricAlgorithm;
import hr.fer.nos.lab2.supported.SymmetricAlgorithm;
import hr.fer.nos.lab2.supported.SymmetricAlgorithmMode;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static hr.fer.nos.lab2.io.CryptoFileField.*;
import static hr.fer.nos.lab2.util.ConverterUtil.*;

public class EnvelopeFile extends CryptoFile {

    private static final List<CryptoFileField> REQUIRED_FIELDS = List.of(
            DESCRIPTION, FILE_NAME, METHOD, KEY_LENGTH, ENVELOPE_DATA, ENVELOPE_CRYPT_KEY
    );

    private Path messagePath;

    private SymmetricAlgorithm symmetricAlgorithm;

    private SymmetricAlgorithmMode symmetricAlgorithmMode;

    private int symmetricKeyLength;

    private AsymmetricAlgorithm asymmetricAlgorithm;

    private int asymmetricKeyLength;

    private byte[] iv;

    private byte[] data;

    private byte[] cryptKey;

    public static EnvelopeFile fromProperties(CryptoFileProperties properties) {
        return new EnvelopeFile(properties);
    }

    private EnvelopeFile(CryptoFileProperties properties) {
        super(properties, CryptoFileDescription.ENVELOPE, REQUIRED_FIELDS);

        this.messagePath = Paths.get(properties.getValue(FILE_NAME.toString()));

        String[] methods = properties.getValue(METHOD.toString()).split("\\n");
        String[] symmetricParts = methods[0].split("-");
        this.symmetricAlgorithm = SymmetricAlgorithm.find(symmetricParts[0]);
        this.symmetricAlgorithmMode = SymmetricAlgorithmMode.find(symmetricParts[1]);
        this.asymmetricAlgorithm = AsymmetricAlgorithm.find(methods[1]);

        String[] keyLengths = properties.getValue(KEY_LENGTH.toString()).split("\\n");
        this.symmetricKeyLength = asInt(keyLengths[0]);
        this.asymmetricKeyLength = asInt(keyLengths[1]);

        this.iv = asBytesHex(properties.getValue(INITIALIZATION_VECTOR.toString()));
        this.data = asBytesBase64(properties.getValue(ENVELOPE_DATA.toString()));
        this.cryptKey = asBytesHex(properties.getValue(ENVELOPE_CRYPT_KEY.toString()));
    }

    public Path getMessagePath() {
        return messagePath;
    }

    public SymmetricAlgorithm getSymmetricAlgorithm() {
        return symmetricAlgorithm;
    }

    public SymmetricAlgorithmMode getSymmetricAlgorithmMode() {
        return symmetricAlgorithmMode;
    }

    public int getSymmetricKeyLength() {
        return symmetricKeyLength;
    }

    public AsymmetricAlgorithm getAsymmetricAlgorithm() {
        return asymmetricAlgorithm;
    }

    public int getAsymmetricKeyLength() {
        return asymmetricKeyLength;
    }

    public byte[] getIv() {
        return iv;
    }

    public byte[] getData() {
        return data;
    }

    public byte[] getCryptKey() {
        return cryptKey;
    }
}
