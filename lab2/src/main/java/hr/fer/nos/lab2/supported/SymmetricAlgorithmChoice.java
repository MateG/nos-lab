package hr.fer.nos.lab2.supported;

import static hr.fer.nos.lab2.supported.SymmetricAlgorithm.AES;
import static hr.fer.nos.lab2.supported.SymmetricAlgorithm.TRIPLE_DES;

public enum SymmetricAlgorithmChoice {

    AES_128(AES, 128),
    AES_192(AES, 192),
    AES_256(AES, 256),
    TRIPLE_DES_128(TRIPLE_DES, 128),
    TRIPLE_DES_192(TRIPLE_DES, 192);

    public final SymmetricAlgorithm algorithm;

    public final int keySize;

    SymmetricAlgorithmChoice(SymmetricAlgorithm algorithm, int keySize) {
        this.algorithm = algorithm;
        this.keySize = keySize;
    }

    public static SymmetricAlgorithmChoice find(String algorithmName, int keySize) {
        for (SymmetricAlgorithmChoice choice : values()) {
            if (choice.algorithm.label.equals(algorithmName) && choice.keySize == keySize) {
                return choice;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return algorithm + "-" + keySize;
    }
}
