package hr.fer.nos.lab2.jfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Lab2Application extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/app.fxml"));
        stage.setScene(new Scene(root, 1280, 720));
        stage.setTitle("NOS - Lab2");
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
