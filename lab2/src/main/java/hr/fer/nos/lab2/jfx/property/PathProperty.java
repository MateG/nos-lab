package hr.fer.nos.lab2.jfx.property;

import javafx.beans.property.SimpleStringProperty;

import java.nio.file.Path;

public class PathProperty extends SimpleStringProperty {

    private Path path;

    public PathProperty() {
    }

    public PathProperty(Path path) {
        setPath(path);
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
        set(path.getFileName().toString());
    }
}
