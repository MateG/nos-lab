package hr.fer.nos.lab2.jfx.control.envelope;

import hr.fer.nos.lab2.jfx.component.choice.SymmetricAlgorithmModeInput;
import hr.fer.nos.lab2.jfx.component.file.FileInput;
import hr.fer.nos.lab2.jfx.component.file.MessageInput;
import hr.fer.nos.lab2.jfx.util.AsyncUtil;
import hr.fer.nos.lab2.jfx.util.FileChooserUtil;
import hr.fer.nos.lab2.service.io.EnvelopeFileService;
import javafx.fxml.FXML;

import java.nio.file.Path;

import static hr.fer.nos.lab2.jfx.util.AlertUtil.showInfo;

public class CreateEnvelopeController {

    @FXML
    private MessageInput messageInput;

    @FXML
    private FileInput secretKeyInput;

    @FXML
    private SymmetricAlgorithmModeInput modeInput;

    @FXML
    private FileInput publicKeyInput;

    @FXML
    public void createAndSave() {
        Path path = FileChooserUtil.save("envelope.txt");
        if (path == null) return;

        AsyncUtil.execute(
                () -> EnvelopeFileService.getInstance().createEnvelope(
                        messageInput.getChosenPath(),
                        secretKeyInput.getChosenPath(),
                        modeInput.getChosenMode(),
                        publicKeyInput.getChosenPath(),
                        path
                ),
                () -> showInfo("Digitalna omotnica uspješno spremljena.")
        );
    }
}
