package hr.fer.nos.lab2.supported;

public enum HashFunction {

    SHA2("SHA"),
    SHA3("SHA3");

    private String label;

    HashFunction(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }
}
