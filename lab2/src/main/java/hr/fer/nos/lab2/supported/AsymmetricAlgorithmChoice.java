package hr.fer.nos.lab2.supported;

import static hr.fer.nos.lab2.supported.AsymmetricAlgorithm.RSA;

public enum AsymmetricAlgorithmChoice {

    RSA_1024(RSA, 1024),
    RSA_2048(RSA, 2048),
    RSA_3072(RSA, 3072),
    RSA_4096(RSA, 4096);

    public final AsymmetricAlgorithm algorithm;

    public final int keySize;

    AsymmetricAlgorithmChoice(AsymmetricAlgorithm algorithm, int keySize) {
        this.algorithm = algorithm;
        this.keySize = keySize;
    }

    public static AsymmetricAlgorithmChoice find(String algorithmName, int keySize) {
        for (AsymmetricAlgorithmChoice choice : values()) {
            if (choice.algorithm.toString().equals(algorithmName) && choice.keySize == keySize) {
                return choice;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return algorithm + "-" + keySize;
    }
}
