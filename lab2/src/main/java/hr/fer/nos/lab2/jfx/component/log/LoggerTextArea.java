package hr.fer.nos.lab2.jfx.component.log;

import hr.fer.nos.lab2.util.Logger;
import javafx.application.Platform;
import javafx.scene.control.TextArea;

public class LoggerTextArea extends TextArea {

    public LoggerTextArea() {
        Logger.getInstance().setConsumer(text -> Platform.runLater(
                () -> appendText(text))
        );
    }
}
