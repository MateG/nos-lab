package hr.fer.nos.lab2.service.crypto;

public class CryptoServiceException extends RuntimeException {

    public CryptoServiceException() {
    }

    public CryptoServiceException(String message) {
        super(message);
    }

    public CryptoServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public CryptoServiceException(Throwable cause) {
        super(cause);
    }

    public CryptoServiceException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
