package hr.fer.nos.lab2.service.io;

import hr.fer.nos.lab2.io.CryptoFileDescription;
import hr.fer.nos.lab2.io.CryptoFileProperties;
import hr.fer.nos.lab2.io.parser.CryptoParser;
import hr.fer.nos.lab2.io.MessageFile;
import hr.fer.nos.lab2.io.impl.RSAPrivateKeyFile;
import hr.fer.nos.lab2.io.impl.RSAPublicKeyFile;
import hr.fer.nos.lab2.io.impl.SignatureFile;
import hr.fer.nos.lab2.service.crypto.SignatureService;
import hr.fer.nos.lab2.supported.HashFunctionChoice;

import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.Map;

import static hr.fer.nos.lab2.io.CryptoFileField.*;
import static hr.fer.nos.lab2.supported.AsymmetricAlgorithm.RSA;
import static hr.fer.nos.lab2.util.ConverterUtil.asHex;

public class SignatureFileService {

    private static SignatureFileService instance = new SignatureFileService();

    public static SignatureFileService getInstance() {
        return instance;
    }

    private SignatureFileService() {
    }

    public void createSignature(Path messagePath,
            HashFunctionChoice hashFunctionChoice, Path privateKeyPath,
            Path savePath) {
        RSAPrivateKeyFile privateKeyFile = RSAPrivateKeyFile.fromProperties(
                new CryptoParser(privateKeyPath).parse()
        );
        byte[] signature = SignatureService.getInstance().generateRSASignature(
                MessageFile.fromPath(messagePath).getMessage(),
                hashFunctionChoice.toString(),
                privateKeyFile.getKey()
        );

        Map<String, String> properties = new LinkedHashMap<>();
        properties.put(DESCRIPTION.toString(), CryptoFileDescription.SIGNATURE.toString());
        properties.put(FILE_NAME.toString(), messagePath.getFileName().toString());
        properties.put(METHOD.toString(), hashFunctionChoice.function + "\n" + RSA);
        properties.put(KEY_LENGTH.toString(), asHex(hashFunctionChoice.size)
                + "\n" + asHex(privateKeyFile.getModulusSize()));
        properties.put(SIGNATURE.toString(), asHex(signature));
        new CryptoFileProperties(properties).save(savePath);
    }

    public boolean verifySignature(Path signaturePath, Path publicKeyPath) {
        SignatureFile signatureFile = SignatureFile.fromProperties(
                new CryptoParser(signaturePath).parse()
        );

        HashFunctionChoice hashFunctionChoice = signatureFile.getHashFunctionChoice();

        RSAPublicKeyFile publicKeyFile = RSAPublicKeyFile.fromProperties(
                new CryptoParser(publicKeyPath).parse()
        );

        return SignatureService.getInstance().verifyRSASignature(
                signatureFile.getSignature(),
                hashFunctionChoice.toString(),
                MessageFile.fromPath(signatureFile.getMessagePath()).getMessage(),
                publicKeyFile.getKey()
        );
    }
}
