package hr.fer.nos.lab2.io.impl;

import hr.fer.nos.lab2.io.*;
import hr.fer.nos.lab2.supported.AsymmetricAlgorithm;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.List;

import static hr.fer.nos.lab2.io.CryptoFileField.*;
import static hr.fer.nos.lab2.util.ConverterUtil.asBigInteger;
import static hr.fer.nos.lab2.util.ConverterUtil.asInt;

public class RSAPublicKeyFile extends CryptoFile {

    private static final List<CryptoFileField> REQUIRED_FIELDS = List.of(
            DESCRIPTION, METHOD, KEY_LENGTH, MODULUS, PUBLIC_EXPONENT
    );

    private int modulusSize;

    private BigInteger modulus;

    private BigInteger publicExponent;

    public static RSAPublicKeyFile fromProperties(CryptoFileProperties properties) {
        if (!AsymmetricAlgorithm.RSA.label.equals(properties.getValue(METHOD.toString()))) {
            throw new CryptoFileException("Expected method: "
                    + AsymmetricAlgorithm.RSA.label + ", but was: "
                    + properties.getValue(METHOD.toString()));
        }

        return new RSAPublicKeyFile(properties);
    }

    private RSAPublicKeyFile(CryptoFileProperties properties) {
        super(properties, CryptoFileDescription.PUBLIC_KEY, REQUIRED_FIELDS);

        this.modulusSize = asInt(properties.getValue(KEY_LENGTH.toString()));
        this.modulus = asBigInteger(properties.getValue(MODULUS.toString()));
        this.publicExponent = asBigInteger(properties.getValue(PUBLIC_EXPONENT.toString()));
    }

    public int getModulusSize() {
        return modulusSize;
    }

    public RSAPublicKey getKey() {
        try {
            return (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(
                    new RSAPublicKeySpec(modulus, publicExponent)
            );
        } catch (GeneralSecurityException e) {
            throw new CryptoFileException("Invalid key parameters: " + e.getMessage());
        }
    }
}
