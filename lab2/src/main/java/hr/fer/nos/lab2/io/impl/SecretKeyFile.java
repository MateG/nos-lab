package hr.fer.nos.lab2.io.impl;

import hr.fer.nos.lab2.io.CryptoFile;
import hr.fer.nos.lab2.io.CryptoFileDescription;
import hr.fer.nos.lab2.io.CryptoFileField;
import hr.fer.nos.lab2.io.CryptoFileProperties;
import hr.fer.nos.lab2.supported.SymmetricAlgorithm;
import hr.fer.nos.lab2.util.ConverterUtil;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.List;

import static hr.fer.nos.lab2.io.CryptoFileField.*;

public class SecretKeyFile extends CryptoFile {

    private static final List<CryptoFileField> REQUIRED_FIELDS = List.of(
            DESCRIPTION, METHOD, SECRET_KEY
    );

    private SymmetricAlgorithm symmetricAlgorithm;

    private byte[] key;

    public static SecretKeyFile fromProperties(CryptoFileProperties properties) {
        return new SecretKeyFile(properties);
    }

    private SecretKeyFile(CryptoFileProperties properties) {
        super(properties, CryptoFileDescription.SECRET_KEY, REQUIRED_FIELDS);

        this.symmetricAlgorithm = SymmetricAlgorithm.find(properties.getValue(METHOD.toString()));
        this.key = ConverterUtil.asBytesHex(properties.getValue(SECRET_KEY.toString()));
    }

    public SymmetricAlgorithm getSymmetricAlgorithm() {
        return symmetricAlgorithm;
    }

    public SecretKey getKey() {
        return new SecretKeySpec(key, symmetricAlgorithm.cLabel);
    }

    public int getKeyLength() {
        return key.length * 2; // 2 characters per byte
    }
}
