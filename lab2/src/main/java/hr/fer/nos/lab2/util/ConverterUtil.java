package hr.fer.nos.lab2.util;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Base64;

public final class ConverterUtil {

    private ConverterUtil() {
    }

    public static String asHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b & 0xff));
        }

        return sb.toString();
    }

    public static String asHex(int value) {
        return asHex(BigInteger.valueOf(value)); // :(
    }

    public static String asHex(BigInteger value) {
        return asHex(value.toByteArray());
    }

    public static String asBase64(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static int asInt(String hex) {
        return Integer.decode("0x" + hex);
    }

    public static BigInteger asBigInteger(String hex) {
        return new BigInteger(hex, 16);
    }

    public static byte[] asBytesHex(String hex) {
        byte[] signed = asBigInteger(hex).toByteArray();
        return signed[0] == 0 ? Arrays.copyOfRange(signed, 1, signed.length) : signed;
    }

    public static byte[] asBytesBase64(String base64) {
        return Base64.getDecoder().decode(base64);
    }
}
