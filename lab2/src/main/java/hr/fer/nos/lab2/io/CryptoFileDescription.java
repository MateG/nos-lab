package hr.fer.nos.lab2.io;

/**
 * Supported values for the "Description" field.
 */
public enum CryptoFileDescription {

    SECRET_KEY("Secret key"),
    PRIVATE_KEY("Private key"),
    PUBLIC_KEY("Public key"),
    SIGNATURE("Signature"),
    ENVELOPE("Envelope");

    private String text;

    CryptoFileDescription(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
