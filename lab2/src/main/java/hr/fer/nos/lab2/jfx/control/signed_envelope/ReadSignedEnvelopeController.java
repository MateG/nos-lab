package hr.fer.nos.lab2.jfx.control.signed_envelope;

import hr.fer.nos.lab2.jfx.component.file.EnvelopeSignatureInput;
import hr.fer.nos.lab2.jfx.component.file.PrivateKeyInput;
import hr.fer.nos.lab2.jfx.component.file.PublicKeyInput;
import hr.fer.nos.lab2.jfx.util.AsyncUtil;
import hr.fer.nos.lab2.service.io.SignedEnvelopeFileService;
import javafx.fxml.FXML;

import static hr.fer.nos.lab2.jfx.util.AlertUtil.showInfo;

public class ReadSignedEnvelopeController {

    @FXML
    public EnvelopeSignatureInput signedEnvelopeInput;

    @FXML
    public PublicKeyInput publicKeyInput;

    @FXML
    public PrivateKeyInput privateKeyInput;

    @FXML
    public void checkAndRead() {
        AsyncUtil.executeChecked(
                () -> SignedEnvelopeFileService.getInstance().verifySignedEnvelope(
                        signedEnvelopeInput.getChosenPath(),
                        publicKeyInput.getChosenPath()
                ),
                () -> showInfo("Potpis nije valjan."),
                () -> SignedEnvelopeFileService.getInstance().readSignedEnvelope(
                        signedEnvelopeInput.getChosenPath(),
                        privateKeyInput.getChosenPath()
                ),
                message -> showInfo("Sadržaj originalne poruke:", message)
        );
    }
}
