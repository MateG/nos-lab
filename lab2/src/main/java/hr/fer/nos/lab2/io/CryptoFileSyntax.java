package hr.fer.nos.lab2.io;

/**
 * Syntactic constants needed for reading/writing crypto files.
 */
public class CryptoFileSyntax {

    public static final String BEGIN_LABEL = "---BEGIN OS2 CRYPTO DATA---";
    public static final String END_LABEL = "---END OS2 CRYPTO DATA---";
    public static final String KEY_SUFFIX = ":";
    public static final String VALUE_PREFIX = "    ";
}
