package hr.fer.nos.lab2.supported;

public enum SymmetricAlgorithm {

    AES("AES", "AES"),
    TRIPLE_DES("3DES", "DESede");

    public final String label;

    public final String cLabel;

    SymmetricAlgorithm(String label, String cLabel) {
        this.label = label;
        this.cLabel = cLabel;
    }

    public static SymmetricAlgorithm find(String label) {
        for (SymmetricAlgorithm algorithm : values()) {
            if (algorithm.label.equals(label)) {
                return algorithm;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return label;
    }
}
