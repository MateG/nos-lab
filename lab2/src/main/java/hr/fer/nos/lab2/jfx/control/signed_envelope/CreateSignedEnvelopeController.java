package hr.fer.nos.lab2.jfx.control.signed_envelope;

import hr.fer.nos.lab2.jfx.component.choice.HashFunctionInput;
import hr.fer.nos.lab2.jfx.component.choice.SymmetricAlgorithmModeInput;
import hr.fer.nos.lab2.jfx.component.file.MessageInput;
import hr.fer.nos.lab2.jfx.component.file.PrivateKeyInput;
import hr.fer.nos.lab2.jfx.component.file.PublicKeyInput;
import hr.fer.nos.lab2.jfx.component.file.SecretKeyInput;
import hr.fer.nos.lab2.jfx.util.AsyncUtil;
import hr.fer.nos.lab2.jfx.util.FileChooserUtil;
import hr.fer.nos.lab2.service.io.SignedEnvelopeFileService;
import javafx.fxml.FXML;

import java.nio.file.Path;

import static hr.fer.nos.lab2.jfx.util.AlertUtil.showError;
import static hr.fer.nos.lab2.jfx.util.AlertUtil.showInfo;

public class CreateSignedEnvelopeController {

    @FXML
    public MessageInput messageInput;

    @FXML
    public SecretKeyInput secretKeyInput;

    @FXML
    public SymmetricAlgorithmModeInput modeInput;

    @FXML
    public PublicKeyInput publicKeyInput;

    @FXML
    public HashFunctionInput hashFunctionInput;

    @FXML
    public PrivateKeyInput privateKeyInput;

    @FXML
    public void createAndSave() {
        showInfo("Prvo odaberite put do željene lokacije omotnice, a zatim i put do željene lokacije potpisa.");

        Path[] paths = FileChooserUtil.saveMultiple("signed_envelope.txt", "signed_envelope_signature.txt");
        if (paths == null) return;

        Path saveEnvelopePath = paths[0];
        Path saveSignaturePath = paths[1];

        if (saveEnvelopePath.equals(saveSignaturePath)) {
            showError("Put do omotnice i put do potpisa moraju biti različiti.");
            return;
        }

        AsyncUtil.execute(
                () -> SignedEnvelopeFileService.getInstance().createSignedEnvelope(
                        messageInput.getChosenPath(),
                        secretKeyInput.getChosenPath(),
                        modeInput.getChosenMode(),
                        publicKeyInput.getChosenPath(),
                        hashFunctionInput.getChosenHashFunction(),
                        privateKeyInput.getChosenPath(),
                        saveEnvelopePath,
                        saveSignaturePath
                ),
                () -> showInfo("Pečat uspješno spremljen.")
        );
    }
}
