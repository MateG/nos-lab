package hr.fer.nos.lab2.io;

import hr.fer.nos.lab2.util.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import static hr.fer.nos.lab2.io.CryptoFileSyntax.*;

/**
 * Properties map wrapper that can be written to a file.
 */
public class CryptoFileProperties {

    public static final int LINE_LENGTH = 60;

    private Map<String, String> properties;

    public CryptoFileProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public boolean containsKey(String key) {
        return properties.containsKey(key);
    }

    public String getValue(String key) {
        return properties.get(key);
    }

    public void save(Path path) {
        Logger.getInstance().logStep("Saving to " + path.getFileName() + "...");

        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            writer.write(BEGIN_LABEL);
            writer.write('\n');

            for (Map.Entry<String, String> property : properties.entrySet()) {
                writer.write(property.getKey());
                writer.write(KEY_SUFFIX);
                writer.write('\n');

                String value = property.getValue();
                String[] parts = value.split("\\n");
                for (String part : parts) {
                    int fullRows = part.length() / LINE_LENGTH;
                    int lastRowLength = part.length() % LINE_LENGTH;

                    int from;
                    for (from = 0; from < fullRows * LINE_LENGTH; from += LINE_LENGTH) {
                        writer.write(VALUE_PREFIX);
                        writer.write(part.substring(from, from + LINE_LENGTH));
                        writer.write('\n');
                    }

                    if (lastRowLength > 0) {
                        writer.write(VALUE_PREFIX);
                        writer.write(part.substring(from));
                        writer.write('\n');
                    }
                }

                writer.write('\n');
            }

            writer.write(END_LABEL);
            writer.write('\n');
        } catch (IOException e) {
            throw new CryptoFileException("IO error while writing to: " + path);
        }
    }
}
