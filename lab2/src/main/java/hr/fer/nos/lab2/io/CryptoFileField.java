package hr.fer.nos.lab2.io;

/**
 * Supported fields.
 */
public enum CryptoFileField {

    DESCRIPTION("Description"),
    FILE_NAME("File name"),
    METHOD("Method"),
    KEY_LENGTH("Key length"),
    SECRET_KEY("Secret key"),
    INITIALIZATION_VECTOR("Initialization vector"),
    MODULUS("Modulus"),
    PUBLIC_EXPONENT("Public exponent"),
    PRIVATE_EXPONENT("Private exponent"),
    SIGNATURE("Signature"),
    DATA("Data"),
    ENVELOPE_DATA("Envelope data"),
    ENVELOPE_CRYPT_KEY("Envelope crypt key");

    private String text;

    CryptoFileField(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
