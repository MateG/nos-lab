package hr.fer.nos.lab2.jfx.control.signature;

import hr.fer.nos.lab2.jfx.component.file.PublicKeyInput;
import hr.fer.nos.lab2.jfx.component.file.SignatureInput;
import hr.fer.nos.lab2.jfx.util.AsyncUtil;
import hr.fer.nos.lab2.service.io.SignatureFileService;
import javafx.fxml.FXML;

import static hr.fer.nos.lab2.jfx.util.AlertUtil.showInfo;

public class VerifySignatureController {

    @FXML
    private SignatureInput signatureInput;

    @FXML
    private PublicKeyInput publicKeyInput;

    @FXML
    public void verify() {
        AsyncUtil.execute(
                () -> SignatureFileService.getInstance().verifySignature(
                        signatureInput.getChosenPath(),
                        publicKeyInput.getChosenPath()
                ),
                ok -> showInfo(ok ? "Potpis je valjan." : "Potpis nije valjan.")
        );
    }
}
