package hr.fer.nos.lab2.jfx.component.file;

import java.nio.file.Paths;

public class MessageInput extends FileInput {

    public MessageInput() {
        super("Poruka", Paths.get("./message.txt"));
    }
}
