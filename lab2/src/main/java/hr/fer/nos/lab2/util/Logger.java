package hr.fer.nos.lab2.util;

import java.util.function.Consumer;

public class Logger {

    private static Logger instance = new Logger();

    public static Logger getInstance() {
        return instance;
    }

    private Consumer<String> consumer;

    private Logger() {
    }

    public void setConsumer(Consumer<String> consumer) {
        this.consumer = consumer;
        consumer.accept("Initialized logger.\n");
    }

    public void log(String line) {
        consumer.accept("\n" + line + "\n");
    }

    public void logStep(String line) {
        consumer.accept(" > " + line + "\n");
    }
}
