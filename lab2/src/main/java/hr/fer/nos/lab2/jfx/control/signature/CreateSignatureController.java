package hr.fer.nos.lab2.jfx.control.signature;

import hr.fer.nos.lab2.jfx.component.choice.HashFunctionInput;
import hr.fer.nos.lab2.jfx.component.file.FileInput;
import hr.fer.nos.lab2.jfx.component.file.MessageInput;
import hr.fer.nos.lab2.jfx.util.AlertUtil;
import hr.fer.nos.lab2.jfx.util.AsyncUtil;
import hr.fer.nos.lab2.jfx.util.FileChooserUtil;
import hr.fer.nos.lab2.service.io.SignatureFileService;
import javafx.fxml.FXML;

import java.nio.file.Path;

public class CreateSignatureController {

    @FXML
    private MessageInput messageInput;

    @FXML
    private HashFunctionInput hashFunctionInput;

    @FXML
    private FileInput privateKeyInput;

    @FXML
    public void signAndSave() {
        Path path = FileChooserUtil.save("signature.txt");
        if (path == null) return;

        AsyncUtil.execute(
                () -> SignatureFileService.getInstance().createSignature(
                        messageInput.getChosenPath(),
                        hashFunctionInput.getChosenHashFunction(),
                        privateKeyInput.getChosenPath(),
                        path
                ),
                () -> AlertUtil.showInfo("Potpis uspješno spremljen.")
        );
    }
}
