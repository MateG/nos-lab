package hr.fer.nos.lab2.jfx.control.keygen;

import hr.fer.nos.lab2.jfx.component.choice.SymmetricAlgorithmInput;
import hr.fer.nos.lab2.jfx.util.AsyncUtil;
import hr.fer.nos.lab2.jfx.util.FileChooserUtil;
import hr.fer.nos.lab2.service.io.KeyFileService;
import javafx.fxml.FXML;

import java.nio.file.Path;

import static hr.fer.nos.lab2.jfx.util.AlertUtil.showInfo;

public class GenerateKeysSymmetricController {

    @FXML
    private SymmetricAlgorithmInput algorithmInput;

    @FXML
    public void generate() {
        Path path = FileChooserUtil.save("secret_key.txt");
        if (path == null) return;

        AsyncUtil.execute(
                () -> KeyFileService.getInstance().createSymmetricKey(
                        algorithmInput.getChosenValue(),
                        path
                ),
                () -> showInfo("Ključ uspješno spremljen.")
        );
    }
}
