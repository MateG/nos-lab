package hr.fer.nos.lab2.service.io;

import hr.fer.nos.lab2.io.CryptoFileDescription;
import hr.fer.nos.lab2.io.CryptoFileProperties;
import hr.fer.nos.lab2.service.crypto.KeyGeneratorService;
import hr.fer.nos.lab2.supported.AsymmetricAlgorithmChoice;
import hr.fer.nos.lab2.supported.SymmetricAlgorithmChoice;
import hr.fer.nos.lab2.util.ConverterUtil;

import javax.crypto.SecretKey;
import java.nio.file.Path;
import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.LinkedHashMap;
import java.util.Map;

import static hr.fer.nos.lab2.io.CryptoFileField.*;
import static hr.fer.nos.lab2.util.ConverterUtil.asHex;

public class KeyFileService {

    private static KeyFileService instance = new KeyFileService();

    public static KeyFileService getInstance() {
        return instance;
    }

    private KeyFileService() {
    }

    public void createSymmetricKey(SymmetricAlgorithmChoice algorithmChoice,
            Path savePath) {
        SecretKey secretKey = KeyGeneratorService.getInstance()
                .generateSecretKey(algorithmChoice.algorithm, algorithmChoice.keySize);

        Map<String, String> properties = new LinkedHashMap<>();
        properties.put(DESCRIPTION.toString(), CryptoFileDescription.SECRET_KEY.toString());
        properties.put(METHOD.toString(), algorithmChoice.algorithm.toString());
        properties.put(SECRET_KEY.toString(), ConverterUtil.asHex(secretKey.getEncoded()));
        new CryptoFileProperties(properties).save(savePath);
    }

    public void createAsymmetricKeys(AsymmetricAlgorithmChoice algorithmChoice,
            Path privateKeySavePath, Path publicKeySavePath) {
        KeyPair keyPair = KeyGeneratorService.getInstance()
                .generateRSAKeyPair(algorithmChoice.keySize);

        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        Map<String, String> properties = new LinkedHashMap<>();
        properties.put(DESCRIPTION.toString(), CryptoFileDescription.PRIVATE_KEY.toString());
        properties.put(METHOD.toString(), algorithmChoice.algorithm.toString());
        properties.put(KEY_LENGTH.toString(), asHex(algorithmChoice.keySize));
        properties.put(MODULUS.toString(), asHex(privateKey.getModulus()));
        properties.put(PRIVATE_EXPONENT.toString(), asHex(privateKey.getPrivateExponent()));
        new CryptoFileProperties(properties).save(privateKeySavePath);

        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        properties = new LinkedHashMap<>();
        properties.put(DESCRIPTION.toString(), CryptoFileDescription.PUBLIC_KEY.toString());
        properties.put(METHOD.toString(), algorithmChoice.algorithm.toString());
        properties.put(KEY_LENGTH.toString(), asHex(algorithmChoice.keySize));
        properties.put(MODULUS.toString(), asHex(publicKey.getModulus()));
        properties.put(PUBLIC_EXPONENT.toString(), asHex(publicKey.getPublicExponent()));
        new CryptoFileProperties(properties).save(publicKeySavePath);
    }
}
