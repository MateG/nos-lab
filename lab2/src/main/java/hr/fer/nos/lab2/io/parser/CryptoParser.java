package hr.fer.nos.lab2.io.parser;

import hr.fer.nos.lab2.io.CryptoFileProperties;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hr.fer.nos.lab2.io.CryptoFileProperties.LINE_LENGTH;
import static hr.fer.nos.lab2.io.CryptoFileSyntax.*;

/**
 * Parses some specified crypto file to {@link CryptoFileProperties}.
 */
public class CryptoParser {

    private Path path;

    public CryptoParser(Path path) {
        this.path = path;
    }

    public CryptoFileProperties parse() {
        List<String> lines = null;
        try {
            lines = Files.readAllLines(path);
        } catch (IOException e) {
            exception("Unable to read file: " + path);
        }

        int beginIndex = lines.indexOf(BEGIN_LABEL);
        if (beginIndex == -1) {
            exception("Begin label not found.");
        }

        int endIndex = lines.indexOf(END_LABEL);
        if (endIndex == -1) {
            exception("End label not found.");
        } else if (endIndex < beginIndex) {
            exception("End label encountered before begin label.");
        }

        return new CryptoFileProperties(parseProperties(lines.subList(beginIndex + 1, endIndex)));
    }

    private Map<String, String> parseProperties(List<String> lines) {
        Map<String, String> properties = new HashMap<>();

        String key = null;
        StringBuilder sb = null;
        for (String line : lines) {
            if (line.endsWith(KEY_SUFFIX)) {
                if (key != null) {
                    if (properties.containsKey(key)) {
                        exception("Key encountered twice: " + key);
                    }

                    properties.put(key, sb.toString());
                }

                key = line.substring(0, line.length() - KEY_SUFFIX.length());
                sb = new StringBuilder();
            } else if (line.startsWith(VALUE_PREFIX)) {
                if (key == null) {
                    exception("Value encountered before key: " + line);
                }

                if (sb.length() % LINE_LENGTH != 0) {
                    sb.append('\n');
                }

                sb.append(line.substring(VALUE_PREFIX.length()));
            } else if (!line.trim().isEmpty()) {
                exception("Encountered neither key or value: " + line);
            }
        }
        if (key != null) properties.put(key, sb.toString());

        return properties;
    }

    private void exception(String message) {
        throw new CryptoParserException(message);
    }
}
