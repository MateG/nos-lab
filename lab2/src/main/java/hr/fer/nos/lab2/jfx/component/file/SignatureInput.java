package hr.fer.nos.lab2.jfx.component.file;

import java.nio.file.Paths;

public class SignatureInput extends FileInput {

    public SignatureInput() {
        super("Potpis", Paths.get("./signature.txt"));
    }
}
