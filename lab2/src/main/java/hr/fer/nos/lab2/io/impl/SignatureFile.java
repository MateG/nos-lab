package hr.fer.nos.lab2.io.impl;

import hr.fer.nos.lab2.io.CryptoFile;
import hr.fer.nos.lab2.io.CryptoFileDescription;
import hr.fer.nos.lab2.io.CryptoFileField;
import hr.fer.nos.lab2.io.CryptoFileProperties;
import hr.fer.nos.lab2.supported.AsymmetricAlgorithmChoice;
import hr.fer.nos.lab2.supported.HashFunctionChoice;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static hr.fer.nos.lab2.io.CryptoFileField.*;
import static hr.fer.nos.lab2.util.ConverterUtil.asBytesHex;
import static hr.fer.nos.lab2.util.ConverterUtil.asInt;

public class SignatureFile extends CryptoFile {

    private static final List<CryptoFileField> REQUIRED_FIELDS = List.of(
            DESCRIPTION, FILE_NAME, METHOD, KEY_LENGTH, SIGNATURE
    );

    private Path messagePath;

    private HashFunctionChoice hashFunctionChoice;

    private AsymmetricAlgorithmChoice algorithmChoice;

    private byte[] signature;

    public static SignatureFile fromProperties(CryptoFileProperties properties) {
        return new SignatureFile(properties);
    }

    private SignatureFile(CryptoFileProperties properties) {
        super(properties, CryptoFileDescription.SIGNATURE, REQUIRED_FIELDS);

        this.messagePath = Paths.get(properties.getValue(FILE_NAME.toString()));

        String[] methods = properties.getValue(METHOD.toString()).split("\\n");
        String[] keyLengths = properties.getValue(KEY_LENGTH.toString()).split("\\n");
        this.hashFunctionChoice = HashFunctionChoice.find(methods[0], asInt(keyLengths[0]));
        this.algorithmChoice = AsymmetricAlgorithmChoice.find(methods[1], asInt(keyLengths[1]));

        this.signature = asBytesHex(properties.getValue(SIGNATURE.toString()));
    }

    public Path getMessagePath() {
        return messagePath;
    }

    public HashFunctionChoice getHashFunctionChoice() {
        return hashFunctionChoice;
    }

    public AsymmetricAlgorithmChoice getAlgorithmChoice() {
        return algorithmChoice;
    }

    public byte[] getSignature() {
        return signature;
    }
}
