package hr.fer.nos.lab2.io;

public class CryptoFileException extends RuntimeException {

    public CryptoFileException() {
    }

    public CryptoFileException(String message) {
        super(message);
    }

    public CryptoFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public CryptoFileException(Throwable cause) {
        super(cause);
    }

    public CryptoFileException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
