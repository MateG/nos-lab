package hr.fer.nos.lab2.jfx.component.file;

import hr.fer.nos.lab2.jfx.property.PathProperty;
import hr.fer.nos.lab2.jfx.util.FileChooserUtil;
import javafx.beans.NamedArg;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;

import java.nio.file.Files;
import java.nio.file.Path;

import static hr.fer.nos.lab2.jfx.util.AlertUtil.showError;

public abstract class FileInput extends TitledPane {

    private static final String CHOOSE_BUTTON_TEXT = "Odaberi iz datoteke...";

    private PathProperty pathProperty = new PathProperty();

    public FileInput(@NamedArg(value = "text") String text, Path path) {
        Button chooseButton = new Button(CHOOSE_BUTTON_TEXT);
        chooseButton.setOnAction(event -> chooseFile());

        Label chosenLabel = new Label();
        chosenLabel.textProperty().bind(pathProperty);

        HBox hBox = new HBox(10.0, chooseButton, chosenLabel);
        hBox.setAlignment(Pos.CENTER_LEFT);

        setText(text);
        setContent(hBox);

        pathProperty.setPath(path);
    }

    public Path getChosenPath() {
        return pathProperty.getPath();
    }

    private void chooseFile() {
        Path path = FileChooserUtil.open(pathProperty.getPath().getFileName().toString());
        if (!Files.isReadable(path)) {
            showError("Datoteka se ne može pročitati.");
            return;
        }

        pathProperty.setPath(path);
    }
}
