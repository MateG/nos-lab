package hr.fer.nos.lab2.service.io;

import hr.fer.nos.lab2.io.CryptoFileDescription;
import hr.fer.nos.lab2.io.CryptoFileProperties;
import hr.fer.nos.lab2.io.parser.CryptoParser;
import hr.fer.nos.lab2.io.MessageFile;
import hr.fer.nos.lab2.io.impl.EnvelopeFile;
import hr.fer.nos.lab2.io.impl.RSAPrivateKeyFile;
import hr.fer.nos.lab2.io.impl.RSAPublicKeyFile;
import hr.fer.nos.lab2.io.impl.SecretKeyFile;
import hr.fer.nos.lab2.service.crypto.EnvelopeService;
import hr.fer.nos.lab2.supported.SymmetricAlgorithmMode;

import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.Map;

import static hr.fer.nos.lab2.io.CryptoFileField.*;
import static hr.fer.nos.lab2.supported.AsymmetricAlgorithm.RSA;
import static hr.fer.nos.lab2.util.ConverterUtil.asBase64;
import static hr.fer.nos.lab2.util.ConverterUtil.asHex;

public class EnvelopeFileService {

    private static EnvelopeFileService instance = new EnvelopeFileService();

    public static EnvelopeFileService getInstance() {
        return instance;
    }

    private EnvelopeFileService() {
    }

    public void createEnvelope(Path messagePath, Path secretKeyPath,
            SymmetricAlgorithmMode mode, Path publicKeyPath, Path savePath) {
        SecretKeyFile secretKeyFile = SecretKeyFile.fromProperties(
                new CryptoParser(secretKeyPath).parse()
        );

        RSAPublicKeyFile publicKeyFile = RSAPublicKeyFile.fromProperties(
                new CryptoParser(publicKeyPath).parse()
        );

        byte[][] envelope = EnvelopeService.getInstance().encryptRSA(
                MessageFile.fromPath(messagePath).getMessage(),
                secretKeyFile.getKey(),
                publicKeyFile.getKey(),
                secretKeyFile.getSymmetricAlgorithm(),
                mode
        );

        Map<String, String> properties = new LinkedHashMap<>();
        properties.put(DESCRIPTION.toString(), CryptoFileDescription.ENVELOPE.toString());
        properties.put(FILE_NAME.toString(), messagePath.getFileName().toString());
        properties.put(METHOD.toString(), secretKeyFile.getSymmetricAlgorithm().label + "-" + mode.label
                + "\n" + RSA.label);
        properties.put(KEY_LENGTH.toString(), asHex(secretKeyFile.getKeyLength())
                + "\n" + asHex(publicKeyFile.getModulusSize()));
        properties.put(INITIALIZATION_VECTOR.toString(), asHex(envelope[0]));
        properties.put(ENVELOPE_DATA.toString(), asBase64(envelope[1]));
        properties.put(ENVELOPE_CRYPT_KEY.toString(), asHex(envelope[2]));
        new CryptoFileProperties(properties).save(savePath);
    }

    public String readEnvelope(Path envelopePath, Path privateKeyPath) {
        EnvelopeFile envelopeFile = EnvelopeFile.fromProperties(
                new CryptoParser(envelopePath).parse()
        );

        RSAPrivateKeyFile privateKeyFile = RSAPrivateKeyFile.fromProperties(
                new CryptoParser(privateKeyPath).parse()
        );

        return new String(EnvelopeService.getInstance().decryptRSA(
                envelopeFile.getIv(),
                envelopeFile.getData(),
                envelopeFile.getCryptKey(),
                privateKeyFile.getKey(),
                envelopeFile.getSymmetricAlgorithm(),
                envelopeFile.getSymmetricAlgorithmMode()
        ));
    }
}
