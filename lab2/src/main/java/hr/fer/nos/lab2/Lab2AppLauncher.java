package hr.fer.nos.lab2;

import hr.fer.nos.lab2.jfx.Lab2Application;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;

public class Lab2AppLauncher {

    public static void main(String[] args) {
        Security.addProvider(new BouncyCastleProvider());
        Lab2Application.main(args);
    }
}
