package hr.fer.nos.lab2.supported;

public enum SymmetricAlgorithmMode {

    CFB("CFB"),
    CTR("CTR");

    public final String label;

    SymmetricAlgorithmMode(String label) {
        this.label = label;
    }

    public static SymmetricAlgorithmMode find(String label) {
        for (SymmetricAlgorithmMode mode : values()) {
            if (mode.label.equals(label)) {
                return mode;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return label;
    }
}
