package hr.fer.nos.lab2.jfx.control.keygen;

import hr.fer.nos.lab2.jfx.component.choice.AsymmetricAlgorithmInput;
import hr.fer.nos.lab2.jfx.util.AsyncUtil;
import hr.fer.nos.lab2.jfx.util.FileChooserUtil;
import hr.fer.nos.lab2.service.io.KeyFileService;
import javafx.fxml.FXML;

import java.nio.file.Path;

import static hr.fer.nos.lab2.jfx.util.AlertUtil.showError;
import static hr.fer.nos.lab2.jfx.util.AlertUtil.showInfo;

public class GenerateKeysAsymmetricController {

    @FXML
    private AsymmetricAlgorithmInput algorithmInput;

    @FXML
    public void generate() {
        showInfo("Prvo odaberite put do željene lokacije privatnog ključa, a zatim i put do željene lokacije javnog ključa.");

        Path[] paths = FileChooserUtil.saveMultiple("private_key.txt", "public_key.txt");
        if (paths == null) return;

        Path privateKeyPath = paths[0];
        Path publicKeyPath = paths[1];

        if (privateKeyPath.equals(publicKeyPath)) {
            showError("Put do privatnog ključa i put do javnog ključa moraju biti različiti.");
            return;
        }

        AsyncUtil.execute(
                () -> KeyFileService.getInstance().createAsymmetricKeys(
                        algorithmInput.getChosenValue(),
                        privateKeyPath,
                        publicKeyPath
                ),
                () -> showInfo("Ključevi uspješno spremljeni.")
        );
    }
}
