package hr.fer.nos.lab2.jfx.component.file;

import java.nio.file.Paths;

public class EnvelopeInput extends FileInput {

    public EnvelopeInput() {
        super("Omotnica", Paths.get("./envelope.txt"));
    }
}
