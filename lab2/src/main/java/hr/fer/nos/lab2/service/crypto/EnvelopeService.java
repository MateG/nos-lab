package hr.fer.nos.lab2.service.crypto;

import hr.fer.nos.lab2.supported.AsymmetricAlgorithm;
import hr.fer.nos.lab2.supported.SymmetricAlgorithm;
import hr.fer.nos.lab2.supported.SymmetricAlgorithmMode;
import hr.fer.nos.lab2.util.Logger;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;

import static org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME;

/**
 * Singleton service used for digital envelope encryption/decryption.
 */
public class EnvelopeService {

    private static EnvelopeService instance = new EnvelopeService();

    public static EnvelopeService getInstance() {
        return instance;
    }

    private EnvelopeService() {
    }

    /**
     * Encrypts the given message using the specified secret key and encrypts
     * the secret key using the specified public key.
     *
     * @param message                The given message.
     * @param secretKey              The specified secret key.
     * @param publicKey              The specified public key.
     * @param symmetricAlgorithm     The specified message encryption algorithm.
     * @param symmetricAlgorithmMode The specified message encryption mode.
     * @return Initialization vector, encrypted message and encrypted key.
     */
    public byte[][] encryptRSA(byte[] message, SecretKey secretKey,
            PublicKey publicKey, SymmetricAlgorithm symmetricAlgorithm,
            SymmetricAlgorithmMode symmetricAlgorithmMode) {
        Logger.getInstance().log("Encrypting RSA envelope...");

        try {
            Cipher sCipher = Cipher.getInstance(
                    symmetricAlgorithm.cLabel + "/" + symmetricAlgorithmMode.label + "/NoPadding",
                    PROVIDER_NAME
            );
            sCipher.init(Cipher.ENCRYPT_MODE, secretKey);

            Logger.getInstance().logStep("Generating initialization vector...");
            byte[] iv = sCipher.getIV();
            Logger.getInstance().logStep("Encrypting message...");
            byte[] data = sCipher.doFinal(message);

            Cipher aCipher = Cipher.getInstance(
                    AsymmetricAlgorithm.RSA.label + "/NONE/NoPadding",
                    PROVIDER_NAME
            );
            aCipher.init(Cipher.ENCRYPT_MODE, publicKey);

            Logger.getInstance().logStep("Encrypting secret key...");
            byte[] cryptKey = aCipher.doFinal(secretKey.getEncoded());

            return new byte[][]{iv, data, cryptKey};
        } catch (GeneralSecurityException e) {
            throw new CryptoServiceException(e.getMessage(), e);
        }
    }

    /**
     * Decrypts the given encrypted secret key using the specified private key
     * and decrypts the given encrypted message data using the given
     * initialization vector and the decrypted secret key.
     *
     * @param iv                     The given initialization vector.
     * @param data                   The given encrypted message data.
     * @param cryptKey               The given encrypted secret key.
     * @param privateKey             The specified private key.
     * @param symmetricAlgorithm     The specified message decryption algorithm.
     * @param symmetricAlgorithmMode The specified message decryption mode.
     * @return Decrypted data.
     */
    public byte[] decryptRSA(byte[] iv, byte[] data, byte[] cryptKey,
            PrivateKey privateKey, SymmetricAlgorithm symmetricAlgorithm,
            SymmetricAlgorithmMode symmetricAlgorithmMode) {
        Logger.getInstance().log("Decrypting RSA envelope...");

        try {
            Logger.getInstance().logStep("Decrypting secret key...");
            Cipher aCipher = Cipher.getInstance(
                    AsymmetricAlgorithm.RSA.label + "/NONE/NoPadding",
                    PROVIDER_NAME
            );
            aCipher.init(Cipher.DECRYPT_MODE, privateKey);
            SecretKey secretKey = new SecretKeySpec(
                    aCipher.doFinal(cryptKey),
                    symmetricAlgorithm.cLabel
            );

            Logger.getInstance().logStep("Decrypting message...");
            Cipher sCipher = Cipher.getInstance(
                    symmetricAlgorithm.cLabel + "/" + symmetricAlgorithmMode.label + "/NoPadding",
                    PROVIDER_NAME
            );
            sCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
            return sCipher.doFinal(data);
        } catch (GeneralSecurityException e) {
            throw new CryptoServiceException(e.getMessage(), e);
        }
    }
}
