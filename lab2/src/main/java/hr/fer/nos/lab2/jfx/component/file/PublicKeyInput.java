package hr.fer.nos.lab2.jfx.component.file;

import java.nio.file.Paths;

public class PublicKeyInput extends FileInput {

    public PublicKeyInput() {
        super("Javni ključ", Paths.get("./public_key.txt"));
    }
}
