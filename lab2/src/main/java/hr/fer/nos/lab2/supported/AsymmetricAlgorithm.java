package hr.fer.nos.lab2.supported;

public enum AsymmetricAlgorithm {

    RSA("RSA");

    public final String label;

    AsymmetricAlgorithm(String label) {
        this.label = label;
    }

    public static AsymmetricAlgorithm find(String label) {
        for (AsymmetricAlgorithm algorithm : values()) {
            if (algorithm.label.equals(label)) {
                return algorithm;
            }
        }

        return null;
    }
}
