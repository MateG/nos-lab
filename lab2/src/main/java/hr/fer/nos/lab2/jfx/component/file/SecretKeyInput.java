package hr.fer.nos.lab2.jfx.component.file;

import java.nio.file.Paths;

public class SecretKeyInput extends FileInput {

    public SecretKeyInput() {
        super("Simetrični (sjednički) ključ", Paths.get("./secret_key.txt"));
    }
}
