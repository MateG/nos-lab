package hr.fer.nos.lab2.jfx.component.choice;

import hr.fer.nos.lab2.supported.HashFunctionChoice;
import javafx.beans.NamedArg;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;

public class HashFunctionInput extends TitledPane {

    private static final String TEXT = "Funkcija sažetka";
    private static final String LABEL_TEXT = "Odaberi:";

    private ChoiceBox<HashFunctionChoice> choiceBox = new ChoiceBox<>();

    public HashFunctionInput(@NamedArg(value = "text", defaultValue = TEXT) String text,
            @NamedArg(value = "labelText", defaultValue = LABEL_TEXT) String labelText) {
        choiceBox.getItems().setAll(HashFunctionChoice.values());
        choiceBox.setValue(HashFunctionChoice.SHA_256);

        HBox hBox = new HBox(10.0, new Label(labelText), choiceBox);
        hBox.setAlignment(Pos.CENTER_LEFT);

        setText(text);
        setContent(hBox);
    }

    public HashFunctionChoice getChosenHashFunction() {
        return choiceBox.getValue();
    }
}
