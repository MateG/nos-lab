package hr.fer.nos.lab2.io.parser;

public class CryptoParserException extends RuntimeException {

    public CryptoParserException() {
    }

    public CryptoParserException(String message) {
        super(message);
    }

    public CryptoParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public CryptoParserException(Throwable cause) {
        super(cause);
    }

    public CryptoParserException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
