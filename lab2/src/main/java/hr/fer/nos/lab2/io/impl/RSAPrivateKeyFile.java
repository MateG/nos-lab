package hr.fer.nos.lab2.io.impl;

import hr.fer.nos.lab2.io.*;
import hr.fer.nos.lab2.supported.AsymmetricAlgorithm;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.RSAPrivateKeySpec;
import java.util.List;

import static hr.fer.nos.lab2.io.CryptoFileField.*;
import static hr.fer.nos.lab2.util.ConverterUtil.asBigInteger;
import static hr.fer.nos.lab2.util.ConverterUtil.asInt;

public class RSAPrivateKeyFile extends CryptoFile {

    private static final List<CryptoFileField> REQUIRED_FIELDS = List.of(
            DESCRIPTION, METHOD, KEY_LENGTH, MODULUS, PRIVATE_EXPONENT
    );

    private int modulusSize;

    private BigInteger modulus;

    private BigInteger privateExponent;

    public static RSAPrivateKeyFile fromProperties(CryptoFileProperties properties) {
        if (!AsymmetricAlgorithm.RSA.label.equals(properties.getValue(METHOD.toString()))) {
            throw new CryptoFileException("Expected method: "
                    + AsymmetricAlgorithm.RSA.label + ", but was: "
                    + properties.getValue(METHOD.toString()));
        }

        return new RSAPrivateKeyFile(properties);
    }

    private RSAPrivateKeyFile(CryptoFileProperties properties) {
        super(properties, CryptoFileDescription.PRIVATE_KEY, REQUIRED_FIELDS);

        this.modulusSize = asInt(properties.getValue(KEY_LENGTH.toString()));
        this.modulus = asBigInteger(properties.getValue(MODULUS.toString()));
        this.privateExponent = asBigInteger(properties.getValue(PRIVATE_EXPONENT.toString()));
    }

    public int getModulusSize() {
        return modulusSize;
    }

    public RSAPrivateKey getKey() {
        try {
            return (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(
                    new RSAPrivateKeySpec(modulus, privateExponent)
            );
        } catch (GeneralSecurityException e) {
            throw new CryptoFileException("Invalid key parameters: " + e.getMessage());
        }
    }
}
