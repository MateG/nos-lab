package hr.fer.nos.lab2.jfx.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static hr.fer.nos.lab2.jfx.util.AlertUtil.showError;
import static java.util.concurrent.CompletableFuture.runAsync;
import static java.util.concurrent.CompletableFuture.supplyAsync;
import static javafx.application.Platform.runLater;

public final class AsyncUtil {

    private static ExecutorService executorService = Executors.newSingleThreadExecutor(
            r -> {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
    );

    private AsyncUtil() {
    }

    /**
     * Executes the given task asynchronously (using a worker thread) and
     * schedules a later action (on success).
     * Handles errors using the {@link javafx.scene.control.Alert} class.
     *
     * @param task           The given task.
     * @param runLaterAction The specified success action.
     */
    public static void execute(Runnable task, Runnable runLaterAction) {
        runAsync(task, executorService)
                .thenRun(() -> runLater(runLaterAction))
                .exceptionally(AsyncUtil::handleError);
    }

    /**
     * Executes the given task asynchronously (using a worker thread) which
     * yields some value that is used in the specified success action.
     * Handles errors using the {@link javafx.scene.control.Alert} class.
     *
     * @param task           The given task.
     * @param runLaterAction The specified success action.
     * @param <T>            Type of the value yielded from the task.
     */
    public static <T> void execute(Supplier<T> task, Consumer<T> runLaterAction) {
        supplyAsync(task, executorService)
                .thenAccept(result -> runLater(() -> runLaterAction.accept(result)))
                .exceptionally(AsyncUtil::handleError);
    }

    /**
     * Performs the specified check task asynchronously (using a worker thread).
     * If the check fails, performs the specified check fail action.
     * If the check passes, executes the given task asynchronously (using a
     * worker thread) which yields some value that is used in the specified
     * success action.
     * Handles errors using the {@link javafx.scene.control.Alert} class.
     *
     * @param checkTask       The specified check task.
     * @param checkFailAction The specified check fail action.
     * @param task            The given task
     * @param runLaterAction  The specified success action.
     * @param <T>             Type of the value yielded from the task.
     */
    public static <T> void executeChecked(Supplier<Boolean> checkTask,
            Runnable checkFailAction, Supplier<T> task,
            Consumer<T> runLaterAction) {
        supplyAsync(checkTask, executorService)
                .thenAcceptAsync(
                        ok -> {
                            if (ok) {
                                T result = task.get();
                                runLater(() -> runLaterAction.accept(result));
                            } else {
                                runLater(checkFailAction);
                            }
                        },
                        executorService
                ).exceptionally(AsyncUtil::handleError);
    }

    private static Void handleError(Throwable throwable) {
        runLater(() -> showError("Dogodila se pogreška!", throwable.getCause().getMessage()));
        return null;
    }
}
