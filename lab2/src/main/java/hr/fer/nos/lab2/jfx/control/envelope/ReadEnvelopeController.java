package hr.fer.nos.lab2.jfx.control.envelope;

import hr.fer.nos.lab2.jfx.component.file.EnvelopeInput;
import hr.fer.nos.lab2.jfx.component.file.PrivateKeyInput;
import hr.fer.nos.lab2.jfx.util.AsyncUtil;
import hr.fer.nos.lab2.service.io.EnvelopeFileService;
import javafx.fxml.FXML;

import static hr.fer.nos.lab2.jfx.util.AlertUtil.showInfo;

public class ReadEnvelopeController {

    @FXML
    private EnvelopeInput envelopeInput;

    @FXML
    private PrivateKeyInput privateKeyInput;

    @FXML
    public void read() {
        AsyncUtil.execute(
                () -> EnvelopeFileService.getInstance().readEnvelope(
                        envelopeInput.getChosenPath(),
                        privateKeyInput.getChosenPath()
                ),
                message -> showInfo("Sadržaj originalne poruke:", message)
        );
    }
}
