package hr.fer.nos.lab2.supported;

import static hr.fer.nos.lab2.supported.HashFunction.SHA2;
import static hr.fer.nos.lab2.supported.HashFunction.SHA3;

public enum HashFunctionChoice {

    SHA_256(SHA2, 256),
    SHA_512(SHA2, 512),
    SHA3_256(SHA3, 256),
    SHA3_512(SHA3, 512);

    public final HashFunction function;

    public final int size;

    HashFunctionChoice(HashFunction function, int size) {
        this.function = function;
        this.size = size;
    }

    public static HashFunctionChoice find(String functionName, int size) {
        for (HashFunctionChoice choice : values()) {
            if (choice.function.toString().equals(functionName) && choice.size == size) {
                return choice;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return function + "-" + size;
    }
}
