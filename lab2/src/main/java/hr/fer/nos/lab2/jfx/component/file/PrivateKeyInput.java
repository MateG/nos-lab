package hr.fer.nos.lab2.jfx.component.file;

import java.nio.file.Paths;

public class PrivateKeyInput extends FileInput {

    public PrivateKeyInput() {
        super("Privatni ključ", Paths.get("./private_key.txt"));
    }
}
