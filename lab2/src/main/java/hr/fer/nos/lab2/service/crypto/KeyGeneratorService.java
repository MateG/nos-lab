package hr.fer.nos.lab2.service.crypto;

import hr.fer.nos.lab2.supported.SymmetricAlgorithm;
import hr.fer.nos.lab2.util.Logger;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.RSAKeyGenParameterSpec;

import static hr.fer.nos.lab2.supported.AsymmetricAlgorithm.RSA;
import static hr.fer.nos.lab2.supported.SymmetricAlgorithm.AES;
import static hr.fer.nos.lab2.supported.SymmetricAlgorithm.TRIPLE_DES;
import static java.security.spec.RSAKeyGenParameterSpec.F4;
import static org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME;

/**
 * Singleton service used for key generation.
 */
public class KeyGeneratorService {

    private static KeyGeneratorService instance = new KeyGeneratorService();

    public static KeyGeneratorService getInstance() {
        return instance;
    }

    private KeyGeneratorService() {
    }

    /**
     * Generates a key of specified size for the specified symmetric algorithm.
     *
     * @param algorithm The specified symmetric algorithm.
     * @param keySize The specified size.
     * @return The generated key.
     */
    public SecretKey generateSecretKey(SymmetricAlgorithm algorithm, int keySize) {
        Logger.getInstance().log("Generating " + algorithm.label + " key...");

        try {
            KeyGenerator generator = KeyGenerator.getInstance(algorithm.cLabel, PROVIDER_NAME);
            generator.init(keySize);
            return generator.generateKey();
        } catch (GeneralSecurityException e) {
            throw new CryptoServiceException(e.getMessage(), e);
        }
    }

    /**
     * Generates an RSA key pair with specified modulus size.
     *
     * @param keySize The specified modulus size.
     * @return The generated private/public key pair.
     */
    public KeyPair generateRSAKeyPair(int keySize) {
        Logger.getInstance().log("Generating RSA key pair...");

        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance(RSA.label, PROVIDER_NAME);
            generator.initialize(new RSAKeyGenParameterSpec(keySize, F4));
            return generator.generateKeyPair();
        } catch (GeneralSecurityException e) {
            throw new CryptoServiceException(e.getMessage(), e);
        }
    }
}
