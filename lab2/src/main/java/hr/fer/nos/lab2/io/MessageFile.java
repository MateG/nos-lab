package hr.fer.nos.lab2.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Plain-text file contents.
 */
public class MessageFile {

    private byte[] message;

    public static MessageFile fromPath(Path path) {
        try {
            return new MessageFile(Files.readAllBytes(path));
        } catch (IOException e) {
            throw new CryptoFileException("Unable to read file: " + path);
        }
    }

    private MessageFile(byte[] message) {
        this.message = message;
    }

    public byte[] getMessage() {
        return message;
    }
}
