package hr.fer.nos.lab2.service.io;

import hr.fer.nos.lab2.io.parser.CryptoParser;
import hr.fer.nos.lab2.io.impl.SignatureFile;
import hr.fer.nos.lab2.supported.HashFunctionChoice;
import hr.fer.nos.lab2.supported.SymmetricAlgorithmMode;

import java.nio.file.Path;

public class SignedEnvelopeFileService {

    private static SignedEnvelopeFileService instance = new SignedEnvelopeFileService();

    public static SignedEnvelopeFileService getInstance() {
        return instance;
    }

    private SignedEnvelopeFileService() {
    }

    public void createSignedEnvelope(Path messagePath, Path secretKeyPath,
            SymmetricAlgorithmMode mode, Path publicKeyPath,
            HashFunctionChoice hashFunctionChoice, Path privateKeyPath,
            Path saveEnvelopePath, Path saveSignaturePath) {
        EnvelopeFileService.getInstance().createEnvelope(
                messagePath,
                secretKeyPath,
                mode,
                publicKeyPath,
                saveEnvelopePath
        );

        SignatureFileService.getInstance().createSignature(
                saveEnvelopePath,
                hashFunctionChoice,
                privateKeyPath,
                saveSignaturePath
        );
    }

    public boolean verifySignedEnvelope(Path signaturePath, Path publicKeyPath) {
        return SignatureFileService.getInstance().verifySignature(
                signaturePath,
                publicKeyPath
        );
    }

    public String readSignedEnvelope(Path signaturePath, Path privateKeyPath) {
        SignatureFile signatureFile = SignatureFile.fromProperties(
                new CryptoParser(signaturePath).parse()
        );

        return EnvelopeFileService.getInstance().readEnvelope(
                signatureFile.getMessagePath(),
                privateKeyPath
        );
    }
}
