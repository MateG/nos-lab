package hr.fer.nos.lab2.jfx.component.choice;

import hr.fer.nos.lab2.supported.SymmetricAlgorithmMode;
import javafx.beans.NamedArg;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;

public class SymmetricAlgorithmModeInput extends TitledPane {

    private static final String TEXT = "Način kriptiranja";
    private static final String LABEL_TEXT = "Odaberi:";

    private ChoiceBox<SymmetricAlgorithmMode> choiceBox = new ChoiceBox<>();

    public SymmetricAlgorithmModeInput(@NamedArg(value = "text", defaultValue = TEXT) String text,
            @NamedArg(value = "labelText", defaultValue = LABEL_TEXT) String labelText) {
        choiceBox.getItems().setAll(SymmetricAlgorithmMode.values());
        choiceBox.setValue(SymmetricAlgorithmMode.CFB);

        HBox hBox = new HBox(10.0, new Label(labelText), choiceBox);
        hBox.setAlignment(Pos.CENTER_LEFT);

        setText(text);
        setContent(hBox);
    }

    public SymmetricAlgorithmMode getChosenMode() {
        return choiceBox.getValue();
    }
}
