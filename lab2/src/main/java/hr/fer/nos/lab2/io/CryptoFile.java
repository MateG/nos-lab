package hr.fer.nos.lab2.io;

import java.util.List;

import static hr.fer.nos.lab2.io.CryptoFileField.DESCRIPTION;

/**
 * Superclass of all crypto files. Contains key-value pairs and performs some
 * specified checks during construction.
 */
public abstract class CryptoFile {

    private CryptoFileProperties properties;

    public CryptoFile(CryptoFileProperties properties,
            CryptoFileDescription description,
            List<CryptoFileField> requiredKeys) {
        this.properties = properties;

        checkAllRequiredKeys(requiredKeys);
        checkDescription(description.toString());
    }

    private void checkAllRequiredKeys(List<CryptoFileField> requiredKeys) {
        for (CryptoFileField key : requiredKeys) {
            if (!properties.containsKey(key.toString())) {
                throw new CryptoFileException("Required field " + key + " not found.");
            }
        }
    }

    private void checkDescription(String expected) {
        String actual = properties.getValue(DESCRIPTION.toString());
        if (!expected.equals(actual)) {
            throw new CryptoFileException("Expected description: " + expected + ", but was: " + actual);
        }
    }
}
