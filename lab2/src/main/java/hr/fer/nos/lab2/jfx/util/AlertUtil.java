package hr.fer.nos.lab2.jfx.util;

import javafx.scene.control.Alert;

public final class AlertUtil {

    private AlertUtil() {
    }

    public static void showInfo(String headerText, String contentText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info");
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }

    public static void showInfo(String message) {
        showInfo(null, message);
    }

    public static void showError(String headerText, String contentText) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Greška");
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }

    public static void showError(String message) {
        showInfo(null, message);
    }
}
