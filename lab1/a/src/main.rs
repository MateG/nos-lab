mod carousel;
mod constants;
mod extern_mq;
mod mq;
mod msg;
mod visitor;

use constants::{MIN_VISITOR, MQ_KEY, VISITOR_COUNT};
use mq::MessageQueue;
use nix::unistd::{fork, ForkResult};
use std::default::Default;

/// Runs when the program starts.
///
/// Creates `VISITOR_COUNT` child visitor processes and simulates the carousel.
fn main() {
    // Create the message queue.
    let mut mq = MessageQueue::new(MQ_KEY, Default::default()).unwrap();

    for id in MIN_VISITOR..MIN_VISITOR + VISITOR_COUNT {
        match fork() {
            Ok(ForkResult::Parent { .. }) => {
                continue;
            }
            Ok(ForkResult::Child) => {
                // Run the visitor process and terminate.
                visitor::run(id, &mut mq);
                return;
            }
            Err(_) => {
                println!("Fork failed");
                mq.remove().unwrap();
                return;
            }
        }
    }

    // Run the carousel process and terminate.
    carousel::run(&mut mq);
    mq.remove().unwrap();
}
