use crate::msg::MsgBuf;
use bitflags::bitflags;
use libc::{c_int, c_long, key_t, size_t, ssize_t};

extern "C" {
    /// ```c
    /// #include <sys/msg.h>
    ///
    /// int msgget(key_t key, int msgflg);
    /// ```
    pub fn msgget(key: key_t, flags: c_int) -> c_int;

    /// ```c
    /// #include <sys/msg.h>
    ///
    /// int msgsnd(int msqid, const void *msgp, size_t msgsz, int msgflg);
    /// ```
    pub fn msgsnd(
        msqid: c_int,
        msgp: *const MsgBuf,
        msgsz: size_t,
        msgflg: c_int,
    ) -> c_int;

    /// ```c
    /// #include <sys/msg.h>
    ///
    /// ssize_t msgrcv(int msqid, void *msgp, size_t msgsz, long msgtyp,
    ///     int msgflg);
    /// ```
    pub fn msgrcv(
        msqid: c_int,
        msgp: *mut MsgBuf,
        msgsz: size_t,
        msgtyp: c_long,
        msgflg: c_int,
    ) -> ssize_t;

    /// ```c
    /// #include <sys/msg.h>
    ///
    /// int msgctl(int msqid, int cmd, struct msqid_ds *buf);
    /// ```
    pub fn msgctl(msqid: c_int, cmd: c_int, buf: *mut u8) -> c_int;
}

bitflags! {
    /// Flags used to construct a message queue.
    pub struct NewMessageQueueFlags: c_int {
        const IPC_CREAT = libc::IPC_CREAT;
        const IPC_EXCL  = libc::IPC_EXCL;

        const USER_READ     = 0o400;
        const USER_WRITE    = 0o200;
        const USER_EXECUTE  = 0o100;
        const GROUP_READ    = 0o040;
        const GROUP_WRITE   = 0o020;
        const GROUP_EXECUTE = 0o010;
        const OTHER_READ    = 0o004;
        const OTHER_WRITE   = 0o002;
        const OTHER_EXECUTE = 0o001;

        const USER_RWX  = Self::USER_READ.bits
                        | Self::USER_WRITE.bits
                        | Self::USER_EXECUTE.bits;
        const GROUP_RWX = Self::GROUP_READ.bits
                        | Self::GROUP_WRITE.bits
                        | Self::GROUP_EXECUTE.bits;
        const OTHER_RWX = Self::OTHER_READ.bits
                        | Self::OTHER_WRITE.bits
                        | Self::OTHER_EXECUTE.bits;
    }
}

impl Default for NewMessageQueueFlags {
    fn default() -> NewMessageQueueFlags {
        NewMessageQueueFlags::IPC_CREAT | NewMessageQueueFlags::USER_RWX
    }
}

bitflags! {
    /// Flags used for sending and receiving messages.
    pub struct SendReceiveFlags: c_int {
        const IPC_NOWAIT  = libc::IPC_NOWAIT;
        const MSG_NOERROR = libc::MSG_NOERROR;
    }
}

impl Default for SendReceiveFlags {
    fn default() -> SendReceiveFlags {
        SendReceiveFlags::empty()
    }
}
