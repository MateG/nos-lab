use crate::constants::{
    CAROUSEL, FINISHED, RIDE_REQUEST, SIT_DOWN, STAND_UP, VISITOR,
    VISITOR_RIDE_COUNT, VISITOR_SLEEP_MAX, VISITOR_SLEEP_MIN,
};
use crate::mq::MessageQueue;
use rand::Rng;
use std::thread;
use std::time::Duration;

/// Rides the carousel `VISITOR_RIDE_COUNT` times.
pub fn run(id: i64, mq: &mut MessageQueue) {
    for _ in 0..VISITOR_RIDE_COUNT {
        sleep_random();

        mq.send_default(CAROUSEL, id, RIDE_REQUEST.as_bytes())
            .unwrap();

        // Expect a `SIT_DOWN` message.
        assert_eq!(mq.receive_default(id).unwrap().text, SIT_DOWN);
        println!("Sjeo posjetitelj {}", id);

        // Expect a `STAND_UP` message.
        assert_eq!(mq.receive_default(id).unwrap().text, STAND_UP);
        println!("Sišao posjetitelj {}", id);
    }

    // Leave the carousel.
    let end_msg = format!("{} {} {}", VISITOR, id, FINISHED);
    println!("{}", end_msg);
    mq.send_default(CAROUSEL, id, end_msg.as_bytes()).unwrap();
}

/// Sleeps for some time (between `VISITOR_SLEEP_MIN` and `VISITOR_SLEEP_MAX`
/// miliseconds).
fn sleep_random() {
    thread::sleep(Duration::from_millis(
        rand::thread_rng().gen_range(VISITOR_SLEEP_MIN, VISITOR_SLEEP_MAX),
    ));
}
