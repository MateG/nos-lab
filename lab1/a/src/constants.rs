/// Key used for message queue identifier retrieval.
pub const MQ_KEY: i32 = 12345;

/// Smallest visitor identifier.
pub const MIN_VISITOR: i64 = 1;
/// Number of visitor processes.
pub const VISITOR_COUNT: i64 = 8;
/// Carousel identifier.
pub const CAROUSEL: i64 = MIN_VISITOR + VISITOR_COUNT;

/// Number of visitors allowed to ride the carousel at once.
pub const CAROUSEL_CAPACITY: u64 = 4;
/// Number of rides each visitor wants to take in total.
pub const VISITOR_RIDE_COUNT: i64 = 3;

/// Lower visitor sleep duration boundary (in ms).
pub const VISITOR_SLEEP_MIN: u64 = 100;
/// Upper visitor sleep duration boundary (in ms).
pub const VISITOR_SLEEP_MAX: u64 = 2000;
/// Lower carousel sleep duration boundary (in ms).
pub const CAROUSEL_SLEEP_MIN: u64 = 1000;
/// Upper carousel sleep duration boundary (in ms).
pub const CAROUSEL_SLEEP_MAX: u64 = 3000;

/// Message text for requesting a carousel ride.
pub const RIDE_REQUEST: &str = "Želim se voziti";
/// Carousel ride approval message text.
pub const SIT_DOWN: &str = "Sjedni";
/// Carousel ride end message text.
pub const STAND_UP: &str = "Ustani";

/// Visitor message text part.
pub const VISITOR: &str = "Posjetitelj";
/// Finished message text part.
pub const FINISHED: &str = "završio";
