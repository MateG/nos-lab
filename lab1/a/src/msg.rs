use libc::{c_long, size_t};
use std::mem::size_of_val;
use std::str::from_utf8;

/// `MsgBuf` size (in bytes).
const BUFFER_SIZE: size_t = 1024;
/// `MsgBuf` text field size (in bytes).
pub const TEXT_SIZE: size_t = BUFFER_SIZE - 2 * std::mem::size_of::<c_long>();

/// Custom message buffer structure used in end-to-end communication.
#[repr(C)]
pub struct MsgBuf {
    /// Type/destination identifier.
    pub mtype: c_long,
    /// Origin identifier.
    pub mfrom: c_long,
    /// Message text buffer.
    pub mtext: [u8; TEXT_SIZE],
}

impl Default for MsgBuf {
    fn default() -> MsgBuf {
        MsgBuf {
            mtype: 0,
            mfrom: 0,
            mtext: [0; TEXT_SIZE],
        }
    }
}

/// Parsed `MsgBuf` structure.
pub struct MsgUtf8 {
    /// Destination identifier.
    pub to: c_long,
    /// Origin identifier.
    pub from: c_long,
    /// Message text.
    pub text: String,
}

impl MsgUtf8 {
    /// Parses the specified `MsgBuf` structure.
    ///
    /// Fails if the message text is invalid UTF-8.
    ///
    /// Note: `size` is the number of bytes used in the `MsgBuf` structure,
    /// excluding the `mtype` field (`mfrom` and part of `mtext`).
    pub fn from_msg_buf(
        buffer: &MsgBuf,
        size: usize,
    ) -> Result<MsgUtf8, String> {
        assert!(size > size_of_val(&buffer.mfrom));
        let text_size = size - size_of_val(&buffer.mfrom);

        match from_utf8(&buffer.mtext[0..text_size]) {
            Ok(v) => Ok(MsgUtf8 {
                to: buffer.mtype,
                from: buffer.mfrom,
                text: v.to_string(),
            }),
            Err(_) => Err("Received invalid UTF-8 message".to_string()),
        }
    }
}
