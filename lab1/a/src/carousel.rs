use crate::constants::{
    CAROUSEL, CAROUSEL_CAPACITY, CAROUSEL_SLEEP_MAX, CAROUSEL_SLEEP_MIN,
    FINISHED, MIN_VISITOR, RIDE_REQUEST, SIT_DOWN, STAND_UP, VISITOR,
    VISITOR_COUNT,
};
use crate::mq::MessageQueue;
use crate::msg::MsgUtf8;
use bit_vec::BitVec;
use rand::Rng;
use std::thread;
use std::time::Duration;

/// Runs the carousel until all visitors leave.
pub fn run(mq: &mut MessageQueue) {
    // IDs of currently boarded visitors.
    let mut onboard = [0; CAROUSEL_CAPACITY as usize];
    // Flags containing information on which visitors left.
    let mut alive = BitVec::from_elem(VISITOR_COUNT as usize, true);

    'running: loop {
        let mut onboard_counter = 0;
        while onboard_counter < CAROUSEL_CAPACITY as usize {
            let msg = mq.receive_default(CAROUSEL).unwrap();

            if msg.text == RIDE_REQUEST {
                // If a ride is requested, board that specific visitor.
                mq.send_default(msg.from, CAROUSEL, SIT_DOWN.as_bytes())
                    .unwrap();
                onboard[onboard_counter] = msg.from;
                onboard_counter += 1;
            } else {
                match validate_visitor_finished_msg(&msg) {
                    Ok(id) => {
                        // Some specific visitor left.
                        let index = id - MIN_VISITOR;
                        alive.set(index as usize, false);

                        if alive.none() {
                            // All visitors left. Stop.
                            print_separator();
                            println!("Svi posjetitelji su završili");
                            break 'running;
                        }
                    }
                    Err(s) => {
                        println!("{}", s);
                    }
                }
            }
        }

        // Take the boarded visitors for a ride.
        spin_carousel(mq, &onboard);
    }

    println!("Vrtuljak se gasi");
}

/// Expects that the specified message can be parsed into a
/// `VISITOR {id} FINISHED` message and, if valid, returns the parsed ID.
///
/// Also, note that the message field `from` must correspond to the parsed ID.
fn validate_visitor_finished_msg(msg: &MsgUtf8) -> Result<i64, String> {
    match parse_msg_text(&msg.text) {
        Ok(parsed_id) => {
            if parsed_id == msg.from {
                Ok(parsed_id)
            } else {
                Err(format!(
                    "Vrtuljak dobio dvosmisleni identifikator: \
                    {} u poruci, {} u zaglavlju",
                    parsed_id, msg.from
                ))
            }
        }
        Err(_) => {
            Err(format!("Vrtuljak dobio nepodržanu poruku: {}", msg.text))
        }
    }
}

/// Parses the given `VISITOR {id} FINISHED` message text into a visitor ID.
fn parse_msg_text(text: &str) -> Result<i64, std::num::ParseIntError> {
    return text
        .trim_start_matches(VISITOR)
        .trim_end_matches(FINISHED)
        .trim()
        .parse();
}

/// Pretends to be spinning, but sleeps instead. After waking up, informs the
/// visitors that they should stand up.
fn spin_carousel(mq: &mut MessageQueue, onboard: &[i64]) {
    println!("Pokrenuo vrtuljak");
    sleep_random();
    print_separator();
    println!("Vrtuljak zaustavljen");

    for id in onboard {
        mq.send_default(*id, CAROUSEL, STAND_UP.as_bytes()).unwrap();
    }
}

/// Sleeps for some time (between `CAROUSEL_SLEEP_MIN` and `CAROUSEL_SLEEP_MAX`
/// miliseconds).
fn sleep_random() {
    thread::sleep(Duration::from_millis(
        rand::thread_rng().gen_range(CAROUSEL_SLEEP_MIN, CAROUSEL_SLEEP_MAX),
    ));
}

/// Prints a separator line (makes the output a bit easier to read).
fn print_separator() {
    println!("--------------------");
}
