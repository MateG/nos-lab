use crate::extern_mq::*;
use crate::msg::*;
use libc::{c_int, c_long, key_t};
use std::mem::size_of_val;

/// Wrapper struct for the message queue identifier.
pub struct MessageQueue {
    msqid: c_int,
    buffer: MsgBuf,
}

impl MessageQueue {
    /// Calls `msgget` using the specified arguments.
    ///
    /// When successful, creates a MessageQueue with the returned identifier.
    pub fn new(
        key: key_t,
        flags: NewMessageQueueFlags,
    ) -> Result<MessageQueue, String> {
        match unsafe { msgget(key, flags.bits()) } {
            -1 => Err(nix::Errno::last().desc().to_string()),
            msqid => Ok(MessageQueue {
                msqid: msqid,
                buffer: Default::default(),
            }),
        }
    }

    /// Calls `msgsnd' using the stored identifier and specified arguments.
    ///
    /// If successful, the specified message has been sent.
    pub fn send(
        &mut self,
        msg_type: c_long,
        msg_from: c_long,
        msg_text: &[u8],
        flags: SendReceiveFlags,
    ) -> Result<(), String> {
        let buffer = &mut self.buffer;
        let text_size = msg_text.len();

        assert!(text_size > 0);
        assert!(text_size <= size_of_val(&buffer.mtext));

        buffer.mtype = msg_type;
        buffer.mfrom = msg_from;
        for i in 0..text_size {
            buffer.mtext[i] = msg_text[i];
        }

        let msgsz = size_of_val(&buffer.mfrom) + text_size;
        match unsafe { msgsnd(self.msqid, buffer, msgsz, flags.bits()) } {
            -1 => Err(nix::Errno::last().desc().to_string()),
            _ => Ok(()),
        }
    }

    /// Delegates to `send` using the default flags.
    pub fn send_default(
        &mut self,
        msg_type: c_long,
        msg_from: c_long,
        msg_text: &[u8],
    ) -> Result<(), String> {
        self.send(msg_type, msg_from, msg_text, Default::default())
    }

    /// Calls `msgrcv' using the stored identifier and specified arguments.
    ///
    /// When successful, returns the message type and the received message.
    pub fn receive(
        &mut self,
        msg_type: c_long,
        flags: SendReceiveFlags,
    ) -> Result<MsgUtf8, String> {
        let buffer = &mut self.buffer;

        match unsafe {
            msgrcv(
                self.msqid,
                buffer,
                size_of_val(buffer) - size_of_val(&buffer.mtype),
                msg_type,
                flags.bits(),
            )
        } {
            -1 => Err(nix::Errno::last().desc().to_string()),
            size => MsgUtf8::from_msg_buf(buffer, size as usize),
        }
    }

    /// Delegates to `receive` using the default flags.
    pub fn receive_default(
        &mut self,
        msg_type: c_long,
    ) -> Result<MsgUtf8, String> {
        self.receive(msg_type, Default::default())
    }

    /// Removes the message queue by calling `msgctl` with `IPC_RMID`.
    pub fn remove(&self) -> Result<(), String> {
        match unsafe { msgctl(self.msqid, libc::IPC_RMID, 0 as *mut u8) } {
            -1 => Err(nix::Errno::last().desc().to_string()),
            _ => Ok(()),
        }
    }
}
