/// Process count input prompt.
pub const COUNT_PROMPT: &str = "Broj filozofa: ";
/// Minimum process count.
pub const COUNT_MIN: u32 = 3;
/// Maximum process count.
pub const COUNT_MAX: u32 = 10;

/// Minimum conference duration (in ms).
pub const CONFERENCE_DURATION_MIN: i32 = 100;
/// Maximum conference duration (in ms).
pub const CONFERENCE_DURATION_MAX: i32 = 4000;

/// Critical section sleep duration (in ms).
pub const CS_SLEEP: u64 = 3000;
