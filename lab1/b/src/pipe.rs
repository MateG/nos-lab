use crate::msg::Message;
use crate::util;
use nix::poll;
use nix::unistd;
use num::FromPrimitive;
use std::os::unix::io;

/// Stores the pipeline's file descriptors.
pub struct Pipe {
    /// File descriptor used only for reading.
    fd_read: io::RawFd,
    /// File descriptor used only for writing.
    fd_write: io::RawFd,
}

impl Pipe {
    /// Creates a new `Pipe`, or fails.
    pub fn new() -> Result<Pipe, String> {
        match unistd::pipe() {
            Ok((fd_r, fd_w)) => Ok(Pipe {
                fd_read: fd_r,
                fd_write: fd_w,
            }),
            Err(e) => Err(e.to_string()),
        }
    }

    /// Returns a `PipeReader` for this pipe.
    pub fn get_reader(&self) -> PipeReader {
        PipeReader::new(self.fd_read)
    }

    /// Returns a `PipeWriter` for this pipe.
    pub fn get_writer(&self) -> PipeWriter {
        PipeWriter::new(self.fd_write)
    }
}

/// Pipe's reading file descriptor abstraction.
pub struct PipeReader {
    /// File descriptor used only for reading.
    fd: io::RawFd,
}

impl PipeReader {
    /// Constructor specifying the file descriptor.
    pub fn new(fd: io::RawFd) -> PipeReader {
        PipeReader { fd: fd }
    }

    /// Blocks until a message is read, and then returns it as a `Message`.
    pub fn read(&self) -> Message {
        let mut buf: [u8; 12] = Default::default();
        unistd::read(self.fd, &mut buf).unwrap();

        return Message {
            mtype: FromPrimitive::from_u32(util::as_u32(&buf[0..4]))
                .unwrap_or_else(Default::default),
            mfrom: util::as_u32(&buf[4..8]),
            mtime: util::as_u32(&buf[8..12]),
        };
    }

    /// Blocks until a message is received or the specified time has passed.
    ///
    /// Returns a positive value if there are messages.
    pub fn poll(&self, millis: i32) -> Result<i32, String> {
        poll::poll(
            &mut [poll::PollFd::new(self.fd, poll::PollFlags::POLLIN)],
            millis,
        )
        .map_err(|e| e.to_string())
    }
}

/// Pipe's writing file descriptor abstraction.
pub struct PipeWriter {
    /// File descriptor used only for writing.
    fd: io::RawFd,
}

impl PipeWriter {
    /// Constructor specifying the file descriptor.
    pub fn new(fd: io::RawFd) -> PipeWriter {
        PipeWriter { fd: fd }
    }

    /// Sends the specified `Message`.
    pub fn write(&self, msg: &Message) {
        let mut buf: [u8; 12] = Default::default();
        util::as_bytes(msg.mtype as u32, &mut buf[0..4]);
        util::as_bytes(msg.mfrom, &mut buf[4..8]);
        util::as_bytes(msg.mtime, &mut buf[8..12]);
        unistd::write(self.fd, &buf).unwrap();
    }
}
