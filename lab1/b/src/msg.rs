use num_derive::FromPrimitive;
use std::fmt;

/// Valid `mtype` values.
#[repr(u32)]
#[derive(Clone, Copy, Debug, Eq, FromPrimitive, PartialEq)]
pub enum MessageType {
    Unknown = 0,
    Request,
    Response,
    Exit,
}

impl Default for MessageType {
    fn default() -> MessageType {
        MessageType::Unknown
    }
}

impl fmt::Display for MessageType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

/// Custom message buffer used for end-to-end communication.
#[repr(C)]
pub struct Message {
    /// Message type.
    pub mtype: MessageType,
    /// Origin identifier.
    pub mfrom: u32,
    /// Timestamp when sent.
    pub mtime: u32,
}

impl Message {
    /// Constructor specifying all field values.
    pub fn new(mtype: MessageType, mfrom: u32, mtime: u32) -> Message {
        Message {
            mtype,
            mfrom,
            mtime,
        }
    }

    /// Constructor that assumes `MessageType::Request`.
    pub fn request(from: u32, time: u32) -> Message {
        Message::new(MessageType::Request, from, time)
    }

    /// Constructor that assumes `MessageType::Response`.
    pub fn response(from: u32, time: u32) -> Message {
        Message::new(MessageType::Response, from, time)
    }
}

impl fmt::Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "(type: {}, from: {}, time: {})",
            self.mtype, self.mfrom, self.mtime
        )
    }
}
