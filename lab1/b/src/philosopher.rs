use crate::constants::{
    CONFERENCE_DURATION_MAX, CONFERENCE_DURATION_MIN, CS_SLEEP,
};
use crate::msg::{Message, MessageType};
use crate::pipe::{PipeReader, PipeWriter};
use rand::Rng;
use std::cmp;
use std::thread;
use std::time::{Duration, Instant};

/// State of the philosopher process.
pub struct Philosopher {
    /// Process index/identifier.
    i: u32,
    /// Process count.
    n: u32,
    /// Local clock value.
    clock: u32,
    /// Cached clock value of the critical section request.
    request_time: u32,
    /// Messages to be responded to after critical section.
    delayed: Vec<Message>,
    /// Reader for incoming messages.
    reader: PipeReader,
    /// Writers for outgoing messages.
    writers: Vec<PipeWriter>,
}

impl Philosopher {
    /// Initializes the philosopher process state.
    pub fn new(
        i: u32,
        reader: PipeReader,
        writers: Vec<PipeWriter>,
    ) -> Philosopher {
        Philosopher {
            i,
            n: writers.len() as u32 + 1,
            clock: 0,
            request_time: 0,
            delayed: Vec::with_capacity(writers.len()),
            reader,
            writers,
        }
    }

    /// Simulates the philosopher.
    pub fn run(&mut self) {
        self.participate_in_conference();
        self.sit_at_table();
        self.participate_in_conference();
    }

    /// Sleeps for some time, but responds to any incoming requests.
    fn participate_in_conference(&mut self) {
        // Generate the total sleep time.
        let mut remaining = rand::thread_rng()
            .gen_range(CONFERENCE_DURATION_MIN, CONFERENCE_DURATION_MAX);

        while remaining > 0 {
            // Sleep until a message arrives or time is up.
            let start = Instant::now();
            let poll_result = self.reader.poll(remaining);
            remaining -= start.elapsed().as_millis() as i32;

            if poll_result.is_ok() && poll_result.unwrap() > 0 {
                // A message arrived.
                let msg = self.receive();

                if msg.mtype == MessageType::Request {
                    self.respond_to(&msg);
                }
            }
        }
    }

    /// Prepares for and runs the critical section.
    fn sit_at_table(&mut self) {
        self.send_request();
        self.await_responses();
        self.run_critical_section();
        self.send_delayed_responses();

        // At this point, it would probably be smart to report to other
        // processes that the current process is done, but assuming the sleep
        // times are constant and as defined in the assignment, this isn't
        // necessary.
    }

    /// Broadcasts the critical section request and stores the clock for later.
    fn send_request(&mut self) {
        self.request_time = self.clock;
        self.broadcast(&Message::request(self.i, self.request_time));
    }

    /// Reads all incoming messages until all responses have arrived.
    fn await_responses(&mut self) {
        let mut response_count = 0;
        while response_count < self.n - 1 {
            let msg = self.receive();

            if msg.mtype == MessageType::Request {
                // A request has arrived.
                if msg.mtime > self.request_time
                    || msg.mtime == self.request_time && msg.mfrom > self.i
                {
                    // The other process has to wait.
                    self.delayed.push(msg);
                } else {
                    // The other process can continue.
                    self.respond_to(&msg);
                }
            } else if msg.mtype == MessageType::Response {
                // A response has arrived.
                response_count += 1;
            }
        }
    }

    /// Pretends to be doing something critical, but actually sleeps.
    fn run_critical_section(&self) {
        println!("Filozof {} je za stolom", self.i);
        thread::sleep(Duration::from_millis(CS_SLEEP));
    }

    /// Responds to all processes that have been waiting.
    fn send_delayed_responses(&self) {
        for msg in &self.delayed {
            self.respond_to(msg);
        }
    }

    /// Updates the clock and returns the received message.
    fn receive(&mut self) -> Message {
        let msg = self.reader.read();
        self.update_clock(msg.mtime);
        self.log_read(&msg);
        return msg;
    }

    /// Writes a response to the given (presumably, request) message.
    fn respond_to(&self, msg: &Message) {
        let response = Message::response(self.i, msg.mtime);
        self.writer_for(msg.mfrom).write(&response);
        self.log_write(&response);
    }

    /// Sends the specified message to all other processes.
    fn broadcast(&self, msg: &Message) {
        for writer in &self.writers {
            writer.write(&msg);
        }

        self.log_broadcast(&msg);
    }

    /// Updates the clock using the current and the given time.
    fn update_clock(&mut self, time: u32) {
        self.clock = cmp::max(self.clock, time) + 1;
    }

    /// Returns a writer for the given index (takes care of off-by-one errors).
    fn writer_for(&self, index: u32) -> &PipeWriter {
        if index > self.i {
            &self.writers[index as usize - 1]
        } else {
            &self.writers[index as usize]
        }
    }

    /// Logs the received message.
    fn log_read(&self, msg: &Message) {
        println!("Filozof {} dobio poruku: {}", self.i, msg);
    }

    /// Logs the sent message.
    fn log_write(&self, msg: &Message) {
        println!("Filozof {} šalje poruku: {}", self.i, msg);
    }

    /// Logs the broadcasted message.
    fn log_broadcast(&self, msg: &Message) {
        println!("Filozof {} šalje svima poruku: {}", self.i, msg);
    }
}
