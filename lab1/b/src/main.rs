mod constants;
mod msg;
mod philosopher;
mod pipe;
mod util;

use crate::philosopher::Philosopher;
use constants::{COUNT_MAX, COUNT_MIN, COUNT_PROMPT};
use nix::sys::wait::wait;
use nix::unistd::{fork, ForkResult};
use pipe::{Pipe, PipeWriter};
use std::io;
use std::io::Write;
use std::process;

/// Runs when the program starts.
///
/// Simulates a critical section problem with custom process count.
fn main() {
    print!("{}", COUNT_PROMPT);
    io::stdout().flush().unwrap();

    let n = read_u32();
    check_range(n, COUNT_MIN, COUNT_MAX);

    simulate_philosophers(&create_pipes(n));
}

/// Reads from stdin, expects an unsigned 32-bit integer and returns it.
fn read_u32() -> u32 {
    let mut input_text = String::new();
    if io::stdin().read_line(&mut input_text).is_err() {
        println!("Read from stdin failed");
        process::exit(1);
    }

    let trimmed = input_text.trim();
    match trimmed.parse::<u32>() {
        Ok(value) => value,
        Err(_) => {
            println!("Expected unsigned integer, but got: {}", trimmed);
            process::exit(1);
        }
    }
}

/// Checks if the given value is inside of given (inclusive) boundaries.
fn check_range(value: u32, min: u32, max: u32) {
    if value < min || value > max {
        println!(
            "Expected integer from [{}, {}], but got: {}",
            min, max, value
        );
        process::exit(1);
    }
}

/// Creates and returns a collection of n pipes, or fails.
fn create_pipes(n: u32) -> Vec<Pipe> {
    let mut pipes = Vec::with_capacity(n as usize);

    for _ in 0..n {
        let pipe = Pipe::new();

        if pipe.is_ok() {
            pipes.push(pipe.unwrap());
        } else {
            println!("Pipe creation failed");
            process::exit(1);
        }
    }

    return pipes;
}

/// Creates one philosopher process for each of the given pipes, starts them,
/// and awaits their execution.
fn simulate_philosophers(pipes: &Vec<Pipe>) {
    for i in 0..pipes.len() {
        match fork() {
            Ok(ForkResult::Parent { .. }) => {
                continue;
            }
            Ok(ForkResult::Child) => {
                Philosopher::new(
                    i as u32,
                    pipes[i].get_reader(),
                    get_writers(pipes, i),
                )
                .run();
                return;
            }
            Err(_) => {
                println!("Fork failed");
                process::exit(1);
            }
        }
    }

    for _ in 0..pipes.len() {
        let _ = wait();
    }
}

/// Returns all pipe writers except for the one at the specified index.
fn get_writers(pipes: &Vec<Pipe>, i: usize) -> Vec<PipeWriter> {
    let mut writers = Vec::with_capacity(pipes.len() - 1);

    for j in 0..pipes.len() {
        if j != i {
            writers.push(pipes[j].get_writer());
        }
    }

    return writers;
}
