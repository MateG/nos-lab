/// Converts the speicifed byte array into an unsigned 32-bit integer.
pub fn as_u32(bytes: &[u8]) -> u32 {
    return (bytes[0] as u32)
        + ((bytes[1] as u32) << 8)
        + ((bytes[2] as u32) << 16)
        + ((bytes[3] as u32) << 24);
}

/// Converts the speicifed unsigned 32-bit integer into bytes and stores them
/// in the specified byte array.
pub fn as_bytes(value: u32, bytes: &mut [u8]) {
    bytes[0] = (value & 0xff) as u8;
    bytes[1] = ((value >> 8) & 0xff) as u8;
    bytes[2] = ((value >> 16) & 0xff) as u8;
    bytes[3] = ((value >> 24) & 0xff) as u8;
}
